
## step 1 -  download & read

1. Download the last four seasons data from goverment [實價登入](https://lvr.land.moi.gov.tw/).
2. Load the data as Dataframe.

---

## step 2 - clean & transform

1. clean and transform "road" & "section".
2. handle garbled / corrupt address.

---

## step 3 - group_by & merge

1. group by "road" & "section" then count.
2. transform "road" & "section" for any count less than 7.